import csv
from pymongo import MongoClient
from models.books import database as db

def add_book():

    data = open("bestsellers-with-categories.csv", encoding='utf-8')
    books = csv.reader(data, delimiter=',')
    next(books)

    for book in books:
        try:
            data = {
                "nama": book[0],
                "pengarang": book[1],
                "tahunterbit": book[5],
                "genre": book[6]
            }
            db.insertBook(data)

        except Exception as e:
            print("Kesalahan pada saat memasukkan data: {}".format(e))
            break

def search_books(params):
    for book in db.searchBookByName(params):
        print(book)

if __name__ == "__main__":
    db = db()
    kata_kunci = "harry"
    search_books(kata_kunci)
    
    db.nosql_db.close()





